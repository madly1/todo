// // Don't open the browser during development
// process.env.BROWSER = "none";

module.exports = ({ env }) => {
   return ({
      mode: env,
      style: {
         postcssOptions: {},
         stylus: {}
      },
      webpack: {
         configure: {
            ignoreWarnings: [{ message: /Failed to parse source map/ }],
         },
      },
      // Adding Server
      devServer: {
         port: 3003,
      },
   })
}

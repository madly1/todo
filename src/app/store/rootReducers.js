import { combineReducers } from '@reduxjs/toolkit';

const appReducers = combineReducers({
});

const rootReducers = (state, action) => {
    if (action.type === 'auth/logoutUser') {
       return appReducers(undefined, action);
    }

    return appReducers(state, action);
};

export default rootReducers;

import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './rootReducers';

if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('./rootReducers', () => {
        const newRootReducer = require('./rootReducers').default;
        store.replaceReducer(newRootReducer.createReducer());
    });
}

const middlewares = [];

if (process.env.NODE_ENV === 'development') {
    const { createLogger } = require('redux-logger');
    const logger = createLogger({
        collapsed: (getState, action, logEntry) => !logEntry.error,
    });
    middlewares.push(logger);
}

const store = configureStore({
    reducer: rootReducer,
    // Setting up middleware: default
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware({
            immutableCheck: false,
            serializableCheck: false,
        }).concat(middlewares),
    devTools: process.env.NODE_ENV === 'development',
});

export default store;

import { Outlet, Navigate, useLocation } from 'react-router-dom';
import { useAppSelector } from 'app/store';
import { AuthSelector } from 'app/store/slices/auth/authSlice';
import { useTranslation } from 'react-i18next';

const UserOnline = ({ children }) => {
   const user = useAppSelector(AuthSelector);
   const { i18n } = useTranslation();
   const location = useLocation();

   if (!user) {
      return <Navigate to={`/${i18n.language}/auth/login`} state={{ location }} replace />;
   }

   return children ? children : <Outlet />;
};

export default UserOnline;

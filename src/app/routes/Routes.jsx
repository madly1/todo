import React from 'react';
import { Routes as Switch, Route, Navigate } from 'react-router-dom';
import { Login, Register } from 'app/pages/Auth';
import { UserOnline, UserOffline } from 'app/routes/private';

const Routes = () => {
   return (
     <Switch>
         <Route path='/' element={<div>Home</div>} />
         <Route
             path='auth/login'
             element={
                 <UserOffline>
                     <Login />
                 </UserOffline>
             }
         />
         <Route
             path='auth/register'
             element={
                 <UserOffline>
                     <Register />
                 </UserOffline>
             }
         />
        <Route path='*' element={<div>Error 404</div>} />
     </Switch>
   );
};

export default Routes;

import React from 'react';
import { Outlet, useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

const Language = props => {
   const dispatch = useDispatch();
   const navigate = useNavigate();
   const location = useLocation();
   const { lang } = useParams();
   const { i18n } = useTranslation();

   React.useEffect(() => {
      if (lang) {
         const langRegExp = /\/en\/|\/ru\/|\/ua\//;

         if (location.pathname.match(langRegExp) && !location.pathname.includes(`/${i18n.language}`)) {
            const newUrl = location.pathname.replace(langRegExp, `/${i18n.language}/`);
            // localStorage.setItem('lang', JSON.stringify(lang));
            // i18n.changeLanguage(lang);
            navigate(newUrl);
         }
      }
   }, [lang]);

   return <Outlet />;
};

export default Language;

import withAppProviders from "./withAppProviders";

const App = () => {
  return (
     <div>Hello, Dinara!</div>
  );
}

export default withAppProviders(App)();

import Provider from 'react-redux/es/components/Provider';
import AdapterMoment from '@mui/lab/AdapterMoment';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { StyledEngineProvider, createTheme, ThemeProvider } from '@mui/material/styles';
import store from 'app/store';
import { BrowserRouter } from "app/routes";
import CssBaseline from "@mui/material/CssBaseline";
import history from '@history';

const theme = createTheme({});

const withAppProviders = Component => props => {
   const WrapperComponent = () => (
      <LocalizationProvider dateAdapter={AdapterMoment}>
         <Provider store={store}>
            <StyledEngineProvider injectFirst>
                <BrowserRouter history={history}>
                    <ThemeProvider theme={theme}>
                        <CssBaseline>
                            <Component {...props} />
                        </CssBaseline>
                    </ThemeProvider>
                </BrowserRouter>
            </StyledEngineProvider>
         </Provider>
      </LocalizationProvider>
   );

   return WrapperComponent;
};

export default withAppProviders;
